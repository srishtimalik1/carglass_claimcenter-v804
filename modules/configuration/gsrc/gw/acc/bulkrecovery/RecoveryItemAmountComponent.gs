package gw.acc.bulkrecovery

uses gw.api.financials.IMoney
uses gw.api.financials.IMutableMoney
uses gw.api.financials.IPairedMoney

uses java.math.BigDecimal

class RecoveryItemAmountComponent implements IPairedMoney, IMutableMoney {
  var _recoveryItem: BulkRecoveryItem_Ext
  construct(recoveryItem: BulkRecoveryItem_Ext) {
    _recoveryItem = recoveryItem
  }

  override property get Amount(): BigDecimal {
    return _recoveryItem.Amount
  }

  override property set Amount(newAmount: BigDecimal) {
    _recoveryItem.Amount = newAmount
  }

  override property get Currency(): Currency {
    return _recoveryItem.Currency
  }

  override property get SecondaryMoneyComponent(): IMoney {
    return null
    //## todo: Implement me
  }

  override function calcSecondaryAmount(p0: BigDecimal): BigDecimal {
    return null
    //## todo: Implement me
  }
}
