package gw.acc.bulkrecovery.enhancements

uses gw.acc.bulkrecovery.BulkRecoveryAmountComponent
uses gw.api.financials.CurrencyAmount
uses gw.api.util.CurrencyUtil
uses gw.api.util.DisplayableException

uses java.lang.Exception
uses java.lang.StringBuffer
uses java.lang.System
uses java.lang.Throwable
uses java.math.BigDecimal
uses java.math.RoundingMode
uses java.util.ArrayList

/**
 * Enhances the functionalities to facilitate some of the UI functionalities
 */
enhancement BulkRecovery_ExtEnhancement: entity.BulkRecovery_Ext {
  function initialize() {
    this.Currency = CurrencyUtil.getDefaultCurrency()
    this.SplitEqually = false
    //bulkRecover.RecoveryItems = Collections.EMPTY_LIST as entity.BulkRecoveryItem_Ext[]
    var zero: CurrencyAmount = BigDecimal.ZERO
    this.BulkRecoveryTotal = zero
    this.BulkRecoveryStatus = BulkRecoveryStatus_Ext.TC_DRAFT
    this.TotalReportingAmount = zero
    this.TotalTransactionAmount = zero
  }

  property get BulkRecoveryComponentAmount(): BulkRecoveryAmountComponent {
    return new BulkRecoveryAmountComponent(this)
  }

  property get TotalCurrencyAmount(): CurrencyAmount {
    var trxAmt = this.TotalTransactionAmount
    var repAmt = this.TotalReportingAmount
    trxAmt.setSecondaryMoneyComponent(repAmt)
    return trxAmt
  }

  public function addNewRecoveryItem(): BulkRecoveryItem_Ext {
    var recoveryItem: BulkRecoveryItem_Ext = new BulkRecoveryItem_Ext(this.Bundle)

    recoveryItem.CostType = this.DefaultCostType
    recoveryItem.RecoveryCategory = this.DefaultRecoveryCategory
    recoveryItem.Currency = this.Currency
    this.addToRecoveryItems(recoveryItem)
    if (this.SplitEqually) {
      recalculateSplitAmounts()
    }
    return recoveryItem
  }

  property get Voidable(): boolean {
    var recoveryItems = this.RecoveryItems
    return (recoveryItems.Count > 0) and recoveryItems.allMatch(\ri -> ri.Voidable)
  }

  property get AllowedToVoid(): boolean {
    var recoveryItems = this.RecoveryItems
    return (recoveryItems.Count > 0) and recoveryItems.allMatch(\ri -> ri.AllowedToVoid)
  }

  public function voidRecovery() {
    var bundle = gw.transaction.Transaction.getCurrent()

    try {
      var recoveryItems = this.RecoveryItems
      for (ri in recoveryItems) {
        ri.voidRecovery()
      }
      this.BulkRecoveryStatus = BulkRecoveryStatus_Ext.TC_VOIDED
      bundle.commit()
    } catch (e: Throwable) {
      throw new DisplayableException(displaykey.BulkRecovery.ErrorMsg(e.Message), e)
    }
  }

  public function recalculateTotalAmount() {
    var items: BulkRecoveryItem_Ext[] = this.RecoveryItems

    if (items.length == 0) {
      return
    }

    var totalTransactionAmount: BigDecimal = BigDecimal.ZERO
    var totalReportingAmount: CurrencyAmount = CurrencyAmount.getStrict(BigDecimal.ZERO,
        CurrencyUtil.getReportingCurrency())

    for (var item in items) {
      totalTransactionAmount = totalTransactionAmount.add(item.Amount)
      totalReportingAmount = totalReportingAmount.addStrict(item.getReportingAmount)
    }

    this.TotalTransactionAmount = CurrencyAmount.getStrict(totalTransactionAmount, this.Currency)
    this.TotalReportingAmount = totalReportingAmount
  }

  /**
   * To split the amount equally among all entries
   */
  public function recalculateSplitAmounts() {
    var items: BulkRecoveryItem_Ext[] = this.RecoveryItems
    if ((items.length > 0) and this.SplitEqually) {
      var totalAmount = this.BulkRecoveryTotal
      if (totalAmount == null) {
        totalAmount = 0
      }

      var totalTransactionAmount: BigDecimal = BigDecimal.ZERO
      var totalReportingAmount: CurrencyAmount = CurrencyAmount.getStrict(BigDecimal.ZERO, CurrencyUtil.getReportingCurrency())
      if (totalAmount >= 0) {
        var totalItems: BigDecimal = new BigDecimal(items.length)
        var amountPerItem: CurrencyAmount = totalAmount.divideStrict(totalItems, RoundingMode.DOWN)
        var remainder: CurrencyAmount = totalAmount.subtractStrict(
            amountPerItem.multiplyStrict(totalItems, RoundingMode.UNNECESSARY))
        var i = 0
        while (i < items.length - 1) {
          items[i].Amount = amountPerItem
          totalTransactionAmount = totalTransactionAmount.add(amountPerItem)
          totalReportingAmount = totalReportingAmount.addStrict(items[i].getReportingAmount)
          i = i + 1
        }
        items[items.length - 1].Amount = (amountPerItem.addStrict(remainder))
        totalTransactionAmount = totalTransactionAmount.add(amountPerItem.addStrict(remainder))
        totalReportingAmount = totalReportingAmount.addStrict(items[items.length - 1].getReportingAmount)
      }
      this.TotalTransactionAmount = CurrencyAmount.getStrict(totalTransactionAmount, this.Currency)
      this.TotalReportingAmount = totalReportingAmount
    }
  }

  function markSubmitted() {
    this.BulkRecoveryStatus = BulkRecoveryStatus_Ext.TC_SUBMITTED
  }

  private function validate(): List<String> {
    var vErrorList = new ArrayList<String>()
    try {
      var recoveryItems = this.RecoveryItems
      for (ri in recoveryItems) {

        if (null != ri.Exposure){
          var vR = ri.Exposure.validate(true)
          if (!vR.Empty) {
            var errB = new StringBuffer()
            foreach (er in vR.Errors) {
              if (er typeis gw.api.validation.GeneralValidation) {
                var gtype = er.Type
                var greason = er.Reason
                errB.append(greason + "(" + gtype + ")")
                errB.append(System.getProperty("line.separator"))
              } else if (er typeis gw.api.validation.FieldValidation) {
                var gtype = er.Type
                var greason = er.Reason
                errB.append(greason + "(" + gtype + ")")
                errB.append(System.getProperty("line.separator"))
              }
            }
            vErrorList.add(displaykey.Web.Financials.BulkRecoveries.ErrorsFound(ri.Claim.ClaimNumber)
                + System.getProperty("line.separator")
                + errB.toString())
          }
        }
      }
    } catch (e: Exception) {
      print(e.StackTraceAsString)
    }
    return vErrorList
  }

  function validateAndSubmit() {
    var recoveryItems = this.RecoveryItems
    if (recoveryItems.IsEmpty) {
      throw new DisplayableException(displaykey.Web.Financials.BulkRecoveries.NoClaimsSelected)
    }
    var vR = validate()
    if (vR.Empty) {
      var bundle = gw.transaction.Transaction.getCurrent()
      var recoveryList = new ArrayList<Recovery>()

      for (ri in recoveryItems) {
        var cur = ri.Currency
        var recAmt = ri.Amount
        var finAmount = gw.api.financials.CurrencyAmount.getStrict(recAmt, cur)
        var claim = ri.Claim


        /* var recovery = claim.createRecovery(this.Payer, ri.CostType, ri.CostCategory,ri.RecoveryCategory,ri.LineCategory,finAmount, "", User.util.CurrentUser )*/

        var rset = claim.newRecoverySet()
        var recovery = rset.newRecovery(ri.Exposure, ri.CostType, ri.CostCategory, ri.Currency)
        recovery.Payer = this.Payer
        recovery.RecoveryCategory = ri.RecoveryCategory
        recovery.Comments = this.Comments
        recovery.Currency = cur

        recovery.Status = null
        recovery.addNewLineItem(finAmount, null, ri.LineCategory)
        recoveryList.add(recovery)
        rset.prepareForCommit()
        recovery.BulkRecoveryItem = ri
      }
      markSubmitted()
      bundle.commit()
    } else {
      var erMsgSB = new StringBuffer()
      foreach (vE in vR) {
        erMsgSB.append(System.getProperty("line.separator"))
        erMsgSB.append(vE)
      }
      throw new DisplayableException(erMsgSB.toString())
    }
  }
}
