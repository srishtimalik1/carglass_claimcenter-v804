package gw.acc.bulkrecovery.enhancements

uses gw.acc.bulkrecovery.RecoveryItemAmountComponent
uses gw.api.database.Query
uses gw.api.financials.CurrencyAmount
uses gw.api.util.CurrencyUtil

uses java.math.BigDecimal
uses java.util.ArrayList

enhancement BulkRecoveryItem_ExtEnhancement: entity.BulkRecoveryItem_Ext {
  public function updateRelatedRecoveryItemFields() {
    var rl = this.ReserveLine

    if (rl != null) {
      this.Claim = rl.Claim
      this.Exposure = rl.Exposure
      this.CostCategory = rl.CostCategory
      this.CostType = rl.CostType
    } else if (rl == null) {
      this.Exposure = null
      this.CostCategory = null
      this.CostType = this.BulkRecovery.DefaultCostType
    }
  }

  property get AmountComponent(): RecoveryItemAmountComponent {
    return new RecoveryItemAmountComponent(this)
  }

  public function setClaimByClaimNumber(clmNumber: String) {
    if (clmNumber == null) {
      return
    }
    var claim = entity.Claim.finder.findClaimByClaimNumber(clmNumber)
    if (this.Claim != claim) {
      if (this.Claim.isNew()) {
        this.getBundle().remove(this.Claim)
      }
      this.CostType = this.BulkRecovery.DefaultCostType
      this.RecoveryCategory = this.BulkRecovery.DefaultRecoveryCategory
      this.Claim = claim
      updateRelatedRecoveryItemFields()
    }
  }

  public function voidRecovery() {
    if (this.Voidable) {
      this.Recovery.voidRecovery()
    }
  }

  property get AllowedToVoid(): boolean {
    return perm.Recovery.void(this.Recovery)
  }

  property get Voidable(): boolean {
    return (this.Recovery != null)
        and (this.Recovery.Status != TransactionStatus.TC_VOIDED)
        and (this.Recovery.Status != TransactionStatus.TC_PENDINGVOID)
        and this.Recovery.Voidable
  }

  property get getReportingAmount(): CurrencyAmount {
    var result: BigDecimal = this.getRecalReportingAmount()
    if (result == null) {
      return null
    } else {
      return CurrencyAmount.getStrict(result, CurrencyUtil.getReportingCurrency())
    }
  }

  function getRecalReportingAmount(): BigDecimal {
    var transactionAmount: CurrencyAmount = this.Amount

    if (transactionAmount == null) {
      return null
    }
    var rate: ExchangeRate = this.BulkRecovery.TransToReportingExchangeRate
    if (rate == null) {
      return transactionAmount
    } else {
      return CurrencyUtil.convertAmount(transactionAmount, transactionAmount.Currency, this.Currency, rate.Rate)
    }
  }

  property get PossibleReserveLines_Ext(): entity.ReserveLine[] {
    if ((null == this.Bundle) || (null == this.Claim)) {
      return {}
    }

    var claim = this.Claim
    var costType = this.BulkRecovery.DefaultCostType

    var recoveryCategory = this.BulkRecovery.DefaultRecoveryCategory

    var bundle = this.Bundle
    if (this.ReserveLine != null) {
      if (this.CostType != null) {
        costType = this.CostType
      }
      if (this.RecoveryCategory != null) {
        recoveryCategory = this.RecoveryCategory
      }
    }

    var rlQuery = Query.make(ReserveLine).compare(ReserveLine#Claim.PropertyInfo.Name, Equals, claim)
    // disable filtering , return all
    /*    if (costType != null) {
          rlQuery = rlQuery.compare(ReserveLine#CostType, Equals, costType)
        }
        if (costCategory != null){
          rlQuery = rlQuery.compare(ReserveLine#CostCategory, Equals, costCategory)
        } */
    var reserveLinesFromFinder = rlQuery.select()
    var reserveLinesToReturn = new ArrayList<ReserveLine>()

    var rlexisting = this.ReserveLine
    for (var rl in reserveLinesFromFinder) {
      if ((rlexisting == null) or (rl != rlexisting)) {
        if (!bundle.InsertedAndUpdatedBeans.contains(rl)) {
          rl = bundle.add(rl)
        }
      }
      reserveLinesToReturn.add(rl)
    }

    return reserveLinesToReturn.toTypedArray()
  }

  private function findExposures(): Exposure[] {
    return this.Claim.OrderedExposures.where(\exposure -> {
      if (exposure != this.Exposure) {
        if (exposure.PrimaryCoverage.hasCategory(this.CostCategory)) {
          // Check validation level
          if (exposure.ValidationLevel == null) {
            // Force validation
            exposure.validate(true)
          }
          // Is the validation level at least ability to pay?
          return exposure.ValidationLevel.Priority <= ValidationLevel.TC_PAYMENT.Priority
        }

        // Cost category doesn't match
        return false
      } else {
        // This exposure is already selected
        return true
      }
    })
  }

  property get PossibleExposures_Ext(): Exposure[] {
    if ((null == this.Bundle) || (null == this.ReserveLine)) {
      return {}
    }
    var exposuresFromFinder = findExposures()
    var bundle = this.Bundle
    var exposuresToReturn = new ArrayList<Exposure>(exposuresFromFinder.Count)
    for (var exposure in exposuresFromFinder) {
      if (!bundle.InsertedAndUpdatedBeans.contains(exposure)) {
        exposuresToReturn.add(bundle.add(exposure))
      } else {
        exposuresToReturn.add(exposure)
      }
    }
    return exposuresToReturn.toTypedArray()
  }

  function cleanupClaim() {
    var claim = this.Claim
    if ((null != claim) && claim.isNew()) {
      this.Claim = null
      this.getBundle().remove(claim)
    }
    this.CostType = this.BulkRecovery.DefaultCostType

    this.RecoveryCategory = this.BulkRecovery.DefaultRecoveryCategory
  }
}
