package gw.acc.bulkrecovery

uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query

class BulkRecoveriesHelper_Ext {
  private construct() {
  }

  static function findAllBulkRecoveries(): IQueryBeanResult<BulkRecovery_Ext> {
    var q = Query.make(BulkRecovery_Ext)
    return q.select()
  }

  static function newBulkRecovery(): BulkRecovery_Ext {
    var bundle = gw.transaction.Transaction.getCurrent()
    var bulkRecovery = new BulkRecovery_Ext(bundle)
    bulkRecovery.initialize()
    return bulkRecovery
  }

  static function verifyClaimPermission(claimNum: String): String {
    var claim = findClaim(claimNum)
    if (claim == null) {
      return displaykey.Web.Financials.BulkPay.InvoiceItem.Alert.ClaimNotFound(claimNum)
    }
    if (!perm.Claim.view(claim)) {
      return displaykey.Web.Financials.BulkPay.Error.NoPermissionForClaim
    }
    return null
  }

  static function findClaim(claimNum: String): Claim {
    return Query.make(Claim).compare(Claim#ClaimNumber.PropertyInfo.Name, Equals, claimNum).select().AtMostOneRow
  }
}
