package gw.acc.bulkrecovery

uses gw.api.financials.IMutableMoney

uses java.math.BigDecimal

class BulkRecoveryAmountComponent implements IMutableMoney {
  var _bulkRecovery: BulkRecovery_Ext
  construct(bulkRecovery: BulkRecovery_Ext) {
    _bulkRecovery = bulkRecovery
  }

  override property get Amount(): BigDecimal {
    return _bulkRecovery.BulkRecoveryTotal
  }

  override property set Amount(newAmount: BigDecimal) {
    _bulkRecovery.BulkRecoveryTotal = newAmount
  }

  override property get Currency(): Currency {
    return _bulkRecovery.Currency
  }
}
