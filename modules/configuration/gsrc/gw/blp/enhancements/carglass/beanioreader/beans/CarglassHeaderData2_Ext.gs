package gw.blp.enhancements.carglass.beanioreader.beans

/**
 * Created with IntelliJ IDEA.
 * Author: Srishti.Malik1
 * CarglassHeaderData2_Ext - Class maps the flat file header 2 data into data objects
 */
public class CarglassHeaderData2_Ext {
  var recordType: String as RecordType
  var correlationID: String as CorrelationID
  var insuredName: String as InsuredName
  var insuredAddress: String as InsuredAddress
  var insuredPostalCode: String as InsuredPostalCode
  var insuredAddressCity: String as InsuredAddressCity
  var vatLiable: String as VatLiable
  var vatNumber: String as VatNumber
  var vatRecovery: String as VatRecovery
  var insuredLanguage: String as InsuredLanguage
  var insuredTelephone: String as InsuredTelephone
}