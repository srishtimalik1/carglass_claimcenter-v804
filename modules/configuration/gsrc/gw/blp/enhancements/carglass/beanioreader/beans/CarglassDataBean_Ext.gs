package gw.blp.enhancements.carglass.beanioreader.beans

uses java.util.Map

/**
 * Author: Srishti.Malik1
 * CarglassDataBean_Ext - Class maps the flat file data into data objects
 */
public class CarglassDataBean_Ext {
  //Footer 0 Data- Contains data of Carglass Belgium and Luxembourg (BeLux) NV
  var f0: Map as F0
  //Header 0 - Contains data of the Carglass BeLux NV branch where the invoice was issued
  var h0: CarglassHeaderData0_Ext as H0
 //Header 1 - Data of debtor
  var h1: Map as H1
 //Header 2 - Data Driver
  var h2: CarglassHeaderData2_Ext as H2
  //Header 3 - Data of Invoice
  var h3: CarglassHeaderData3_Ext as H3
  //Header 4 - Filler
  var h4: Map as H4
  //Header 5 - Invoice Amounts
  var h5: Map as H5
  //Header 6 - Data regarding accident description
  var h6: Map as H6
  //Data 0 - Contains data from the invoice in text lines
  var d0: List<Map> as D0
 //Data 1 - Filler
  var d1: List<Map> as D1
  //Data 2 - Invoice rules
  var d2: List<Map> as D2
  //Footer 8-  Filler
  var f8: Map as F8
  //Footer 9- Total Rule
  var f9: Map as F9
}