package gw.blp.enhancements.carglass.constants

/**
 * CarglassConstants_Ext interface defines constants required for Carglass Accelerator
 * Author: Srishti.Malik1
 */
public interface CarglassConstants_Ext {
  public static final var RECORD_STREAM: String = "recordStream"
  public static final var RECORD: String = "record"
  public static final var HQ_TAX_ID: String = "headquartersTaxID"
  public static final var ERROR_MESSAGE_KEY: String = "errorMessage"
  public static final var IS_PROCESS: String = "isProcess"
  public static final var DEBTOR_BALANCE: String = "debtorBalance"
  public static final var INVALID_POLICY: String = "InvalidPolicy"
  public static final var CONTACT_TAX_ID: String = "contactTaxID"
  public static final var FNOL_CALLER: String = "FNOLcaller"
  public static final var CARGLASS: String = "Carglass"
  public static final var CARGLASS_REFERENCE: String = "CarglassReference"
  public static final var INVALID_VIN: String = "InvalidVIN"
  public static final var VALID_VIN: String = "ValidVIN"
  public static final var POLICY_NOT_FOUND: String = "PolicyNotfound"
  public static final var BUSINESS: String = "Business"
  public static final var SIMPLE_DATE_FORMAT: String = "yyyy-MM-dd"
  public static final var EDINumber: String = "EDINumber"
  public static final var STAGING: String = "Staging"
  public static final var RECORD_F0: String = "f0"
  public static final var RECORD_F9: String = "f9"
  public static final var ERROR: String = "Error"
  public static final var PROCESSING_ERROR: String = "ProcessingError"
  public static final var PROCESSING_IN_PROGRESS: String = "ProcessingInProgress"
  public static final var RE_PROCESSING_ERROR: String = "ReProcessingError"
  public static final var FILE_COMPLETED: String = "FileCompleted"
  public static final var YES_Y: String = "Y"
  public static final var NO_N: String = "N"
  public static final var COMMA: String = ","
  public static final var HYPHEN: String = "-"
  public static final var EMPTY_STRING: String = ""
  public static final var INT_ZERO: int = 0
  public static final var INT_ONE: int = 1
  public static final var INT_TWO: int = 2
  public static final var INT_THREE: int = 3
  public static final var INT_SIX: int = 6
}