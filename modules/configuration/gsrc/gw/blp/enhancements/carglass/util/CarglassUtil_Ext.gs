package gw.blp.enhancements.carglass.util

uses gw.blp.enhancements.carglass.acordmapper.CarglassBulkInvoiceMapper_Ext
uses gw.blp.enhancements.carglass.acordmapper.CarglassBulkRecoveryMapper_Ext
uses gw.blp.enhancements.carglass.acordmapper.CarglassClaimMapper_Ext
uses gw.blp.enhancements.carglass.beanioreader.beans.CarglassDataBean_Ext
uses gw.blp.enhancements.carglass.constants.CarglassConstants_Ext
uses gw.pl.persistence.core.Bundle
uses gw.webservice.cc.cc801.claim.ClaimAPI
uses gw.webservice.cc.cc801.financials.bulkpay.BulkInvoiceAPI
uses org.beanio.StreamFactory
uses org.slf4j.LoggerFactory

uses java.io.File
uses java.lang.Exception
uses java.lang.StringBuilder
uses java.util.HashMap
uses java.util.Map
uses gw.api.util.ConfigAccess

/**
 * Main class to read the source file received from Carglass which is used to create Claims.
 * Autho: Srishti.Malik1
 * Date: 5/5/16
 *
 */
public class CarglassUtil_Ext {
  private static var LOGGER = LoggerFactory.getLogger("carglassDailyFileLog")
  private static var _contactTaxID: String as ContactTaxID
  private static var _checkNumber: String as CheckNumber
  private static var _fileName: String as FileName
  private static var _bankAccountNumber : String as BankAccountNumber
  /**
   * function importFile()- is the main method which read the file content, validate the data, process the data and then create claims and bulk invoice.
   * @Param eachFile File received from Carglass which is to be processed
   */
  public function importFile(eachFile: File) {
    try {
      var validationMap = new HashMap<String, Object>()
      var factory = StreamFactory.newInstance()
      factory.load(ConfigAccess.getConfigFile("gsrc/gw/blp/enhancements/carglass/beanioreader/mappingxml/CarglassRecordsMapping_Ext.xml"))
      //Static path, Never change
      var inputFile = factory.createReader(CarglassConstants_Ext.RECORD_STREAM, eachFile);
      FileName = eachFile.Name
      var record = inputFile.read()
      while (record != null) {
        if (inputFile.RecordName.equalsIgnoreCase(CarglassConstants_Ext.RECORD_F0)) {
          var fileData = record as Map
          var hqTaxID = (fileData.get(CarglassConstants_Ext.HQ_TAX_ID) as StringBuilder).substring(CarglassConstants_Ext.INT_TWO) as StringBuilder
          hqTaxID = hqTaxID.insert(CarglassConstants_Ext.INT_THREE, CarglassConstants_Ext.HYPHEN)
          ContactTaxID = hqTaxID.insert(CarglassConstants_Ext.INT_SIX, CarglassConstants_Ext.HYPHEN) as String
          BankAccountNumber = "BE87${fileData.get("bankDataAccountNumber").toString().trim()}"
          LOGGER.debug("Carglass HeadQuarter Tax ID: " + ContactTaxID)
        } else if (inputFile.RecordName.equalsIgnoreCase(CarglassConstants_Ext.RECORD_F9)) {
          var footerData = record as Map
          CheckNumber = footerData.get(CarglassConstants_Ext.EDINumber) as String
        } else if (inputFile.RecordName.equalsIgnoreCase(CarglassConstants_Ext.RECORD)){
          var claimBean = record as CarglassDataBean_Ext
          validationMap = CarglassValidator_Ext.isDataValid(record)
          var errorMessage = validationMap.get(CarglassConstants_Ext.ERROR_MESSAGE_KEY)
          if (null != errorMessage) {
            errorMessage = errorMessage.toString()
          } else {
            errorMessage = null
          }

          var claimDataEntity = CarglassValidator_Ext.CGReferenceExist(claimBean.H3.Vin, claimBean.H3.LossDate, claimBean.H3.CarglassReference, FileName)
          if (null == claimDataEntity) {
            gw.transaction.Transaction.runWithNewBundle(\transactionBundle -> {
              transactionBundle.add(claimDataEntity)
              var newClaim = populateClaimDataStaging(FileName, errorMessage as String, record, transactionBundle)
              var invoiceEntity = populateInvoiceDataStaging(claimBean.H5.get(CarglassConstants_Ext.DEBTOR_BALANCE) as String, claimBean.H3.CorrelationID, FileName, transactionBundle)
              newClaim.addToCarglassInvoices(invoiceEntity)
              claimDataEntity = newClaim
              if (validationMap.get(CarglassConstants_Ext.IS_PROCESS)as Boolean){
                var claim = createClaim(claimBean, claimDataEntity, ContactTaxID)
                claim.ClaimSource = typekey.ClaimSource.TC_CARGLASS
              }
            }, Carglass.carglass.integration.user)
          } else {
            var invoiceDataEntity = CarglassValidator_Ext.CGInvoiceExist(claimBean.H3.CorrelationID, FileName)
            if (null == invoiceDataEntity){
              gw.transaction.Transaction.runWithNewBundle(\transactionBundle -> {
                transactionBundle.add(claimDataEntity)
                var invoiceEntity = populateInvoiceDataStaging(claimBean.H5.get(CarglassConstants_Ext.DEBTOR_BALANCE) as String, claimBean.H3.CorrelationID, FileName, transactionBundle)
                claimDataEntity.addToCarglassInvoices(invoiceEntity)
                claimDataEntity.ClaimNumber.Claim.Exposures.first().Incident.Description = "${claimDataEntity.ClaimNumber.Claim.Exposures.first().Incident.Description}, ${claimBean.H3.CorrelationID} "
              }, Carglass.carglass.integration.user)
              LOGGER.info(" ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA} ${displaykey.Carglass.Claim.InvoiceNumber}: ${claimBean.H3.CorrelationID},  ${displaykey.Carglass.Label.H5Data}: ${displaykey.Carglass.Label.TotalAmount}: ${claimBean.H5.get("totalAmount").toString().trim()}, ${displaykey.Carglass.Label.InsuredBalance}: ${claimBean.H5.get("insuredBalance").toString().trim()}, ${displaykey.Carglass.Label.DebtorBalance}: ${ claimBean.H5.get(CarglassConstants_Ext.DEBTOR_BALANCE).toString().trim()} ")
            }
          }
        }
        record = inputFile.read()
      }
      createBulkInvoice(FileName)
      createBulkRecovery()
      updateClaimStatus()
    } catch (e: Exception) {
      LOGGER.error("Exception in importFile(): ${e.StackTraceAsString}")
      throw e
    }
  }

  /**
   * function populateClaimDataStaging()- Inserts flat file data into CarglassDataImportStaging_Ext table.
   * @Param fileName Name of the received file.
   * @Param errorMessage Contains the error messages
   * @Param record Contains the source file data received from Carglass
   * @Param transactionBundle Transaction object
   * @return CarglassDataImportStaging_Ext  entity object
   */
  private function populateClaimDataStaging(fileName: String, errorMessage: String, record: Object, transactionBundle: Bundle): CarglassDataImportStaging_Ext {
    var claimEntity = transactionBundle.add(new CarglassDataImportStaging_Ext())
    try {
      var claimBean = record as CarglassDataBean_Ext
      claimEntity.CarglassReferenceNumber = claimBean.H3.CarglassReference
      claimEntity.CorrelationID = claimBean.H0.CorrelationID
      claimEntity.PolicyNumber = claimBean.H3.PolicyNumber
      if (null != claimBean.H3.LossDate){
        claimEntity.LossDate = claimBean.H3.LossDate.toSQLDate()
      }
      if (null != claimBean.H3.InvoiceReportedDate){
        claimEntity.ReportedDate = claimBean.H3.InvoiceReportedDate.toSQLDate()
      }
      claimEntity.VIN = claimBean.H3.Vin
      claimEntity.VATLiable = claimBean.H2.VatLiable
      claimEntity.Language = claimBean.H2.InsuredLanguage
      claimEntity.InvoiceOrCreditNote = claimBean.H3.InvoiceOrCreditNote
      claimEntity.FileName = fileName
      claimEntity.ErrorMessage = errorMessage
      if (null != errorMessage) {
        claimEntity.Status = CarglassConstants_Ext.ERROR
      } else {
        claimEntity.Status = CarglassConstants_Ext.STAGING
      }
    } catch (e: Exception) {
      LOGGER.error("Exception in populateClaimDataStaging() ${e.StackTraceAsString}")
      throw e
    }
    return claimEntity
  }

  /**
   * function populateInvoiceDataStaging()- Inserts flat file data into CarglassInvoiceStaging table.
   * @Param amount Invoice Amount received in source file
   * @Param correlationID Unique id received in source file for each claim data
   * @Param transactionBundle Transaction object
   * @return CarglassInvoiceStaging  entity object
   */
  private function populateInvoiceDataStaging(amount: String, correlationID: String, fileName: String, transactionBundle: Bundle): CarglassInvoiceStaging_Ext {
    var invoiceEntity = transactionBundle.add(new CarglassInvoiceStaging_Ext())
    try {
      invoiceEntity.CorrelationID = correlationID
      invoiceEntity.Amount = amount.trim()
      invoiceEntity.FileName = fileName
    } catch (e: Exception) {
      LOGGER.error("Exception in populateInvoiceDataStaging() ${e.StackTraceAsString}")
    }
    return invoiceEntity
  }

  /**
   * function createClaim()- Prepares acordXML object using source file data
   * Create a new claim by sending populated acordXML object to ClaimAPI.
   * @Param CarglassDataBean_Ext flat file data object
   * @Param CarglassDataImportStaging_Ext claim staging entity object
   * @Param taxID Carglass headquarter tax ID to be used for searching contact details from Contact Manager
   * @return Claim  entity object
   */
  private function createClaim(claimBean: CarglassDataBean_Ext, claimEntity: CarglassDataImportStaging_Ext, taxID: String): Claim {
    var claim: Claim

    try {
      var cgAcordXML = new CarglassClaimMapper_Ext(claimBean, taxID).AcordXML
      var claimPublicID = new ClaimAPI().importAcordClaimFromXML(cgAcordXML)
      claim = getClaimEntity(claimPublicID)
      claimEntity.ClaimNumber = claim
      claimEntity.Status = typekey.ClaimState.TC_DRAFT.Code
      var infoMessage = displaykey.Carglass.Label.VerifiedPolicy
      if (!claim.Policy.Verified)  {
        claimEntity.Status = CarglassConstants_Ext.INVALID_POLICY
        infoMessage = displaykey.Carglass.Label.UnverifiedPolicy
      }
      LOGGER.info(" ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference}, ${displaykey.Carglass.Label.VehicleChassisNumber} ${claimBean.H3.Vin} & ${displaykey.Carglass.Label.DateOfLoss}: ${claimBean.H3.LossDate.toSQLDate()} ${CarglassConstants_Ext.SIMPLE_DATE_FORMAT} , ${displaykey.Carglass.Label.NewClaim}: ${claim.ClaimNumber}  ${displaykey.Carglass.Label.CreatedOn} ${infoMessage}: ${claimBean.H3.PolicyNumber}")
      LOGGER.info(" ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA} ${displaykey.Carglass.Claim.InvoiceNumber}: ${claimBean.H3.CorrelationID}, ${displaykey.Carglass.Label.H5Data}: ${displaykey.Carglass.Label.TotalAmount}: ${claimBean.H5.get("totalAmount").toString().trim()}, ${displaykey.Carglass.Label.InsuredBalance}: ${claimBean.H5.get("insuredBalance").toString().trim()}, ${displaykey.Carglass.Label.DebtorBalance}: ${ claimBean.H5.get(CarglassConstants_Ext.DEBTOR_BALANCE).toString().trim()} ")
    } catch (e: Exception) {
      LOGGER.error("Exception in createClaim(). Exception is:  ${e.StackTraceAsString}")
    }
    return claim
  }

  /**
   * function getClaimEntity()- Fetches the Claim entity object for the newly created Claim
   * @Param claimPublicID Newly created Claim's public ID
   * @return Claim  entity object
   */
  private function getClaimEntity(claimPublicID: String): Claim {
    var claim: Claim
    try {
      claim = gw.api.database.Query.make(Claim).compare(Claim#PublicID, Equals, claimPublicID).select().AtMostOneRow
    } catch (e: Exception) {;
      LOGGER.error("Exception in getClaimEntity(). Exception is: ${e.StackTraceAsString}")
    }
    return claim
  }

  /**
   * function createBulkInvoice()- Populates Bulk Invoice DTO
   * Creates Bulk Invoice for all the verified claims.
   * After Bulk invoice creation, submits the invoice for approval.
   * @Param fileName  Name of the received file.
   */
  private function createBulkInvoice(fileName: String) {
    try {
      var bulkInvoiceDTO = new CarglassBulkInvoiceMapper_Ext().prepareBulkInvoiceDTO(ContactTaxID, fileName,BankAccountNumber)
      if (bulkInvoiceDTO.NewInvoiceItems.Count != 0){
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          var bulkInvoice = new BulkInvoiceAPI().createBulkInvoice(bulkInvoiceDTO)
          LOGGER.debug("Bulk Invoice public id: ${bulkInvoice}")
          var invoice = gw.api.database.Query.make(entity.BulkInvoice).compare(BulkInvoice#PublicID, Equals, bulkInvoice).select().AtMostOneRow
          invoice = bundle.add(invoice)
          invoice.BankName = invoice.BankName == null ? "ING België/ING Belgique/ING Belgium" : invoice.BankName
              invoice.validate()
          if (invoice.Valid) {
            invoice.submitForApproval()
            invoice.requestInvoice()
          }
        }, Carglass.carglass.integration.user)
      }
    } catch (e: Exception) {
      LOGGER.error("Exception in createBulkInvoice(). Exception is: ${e.StackTraceAsString}")
    }
  }

  private function createBulkRecovery() {
    try {
      var recoveryClaims = CarglassBulkRecoveryMapper_Ext._bulkRecoveryClaimList
      if (recoveryClaims.Count != 0){
        new CarglassBulkRecoveryMapper_Ext().createBulkRecovery(CheckNumber)
      }
    } catch (e: Exception) {
      LOGGER.error("Exception in createBulkRecovery().Exception is: ${e.StackTraceAsString}")
      throw e
    }
  }

  /**
   * function updateClaimStatus()- Updates Claim Status to "Open" in CarglassDataImportStaging_Ext table for all the Claims whose invoice has been created.
   */
  private function updateClaimStatus() {
    try {
      var claimStagingList = CarglassBulkInvoiceMapper_Ext._claimStagingList
      if (claimStagingList.Count != 0){
        claimStagingList.each(\claimStg -> {
          gw.transaction.Transaction.runWithNewBundle(\updateBundle -> {
            var claims = gw.api.database.Query.make(CarglassDataImportStaging_Ext).and(\andCondition -> {
              andCondition.compare(CarglassDataImportStaging_Ext#CarglassReferenceNumber, Equals, claimStg.CarglassReferenceNumber)
              andCondition.compare(CarglassDataImportStaging_Ext#FileName, Equals, FileName)
              andCondition.compare(CarglassDataImportStaging_Ext#Status, Equals, typekey.ClaimState.TC_DRAFT.Code)
            }).select()
            claims.each(\eachClaim -> {
              updateBundle.add(eachClaim)
              eachClaim.Status = typekey.ClaimState.TC_OPEN.Code
            })
          }, Carglass.carglass.integration.user)
        })
      }
    } catch (e: Exception) {
      LOGGER.error("Exception in updateClaimStatus(). Exception is : ${e.StackTraceAsString}")
      throw  e
    }
  }
}