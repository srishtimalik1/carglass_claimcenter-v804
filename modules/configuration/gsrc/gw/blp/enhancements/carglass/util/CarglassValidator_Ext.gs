package gw.blp.enhancements.carglass.util

uses gw.blp.enhancements.carglass.beanioreader.beans.CarglassDataBean_Ext
uses gw.blp.enhancements.carglass.constants.CarglassConstants_Ext
uses org.slf4j.LoggerFactory

uses java.lang.Exception
uses java.util.ArrayList
uses java.util.Date
uses java.util.HashMap

/**
 * Author: Srishti.Malik1
 * CarglassValidator_Ext: Validate the Carglass's flat file data. log an error if business rule violates else continue with data processing.
 */
public class CarglassValidator_Ext {
  private static var _logger = LoggerFactory.getLogger("carglassDailyFileLog")
  /**
   * function CGReferenceExist()- Check whether carglass reference is already processed or not. If already processed then do not create a new claim.
   * @Param VIN  Vehicle Identification Number
   * @Param lossDate Loss date
   * @Param CGReferenceNumber Unique identifier for each claim data sent in source file.
   * @Param fileName File name
   * @Return CarglassDataImportStaging_Ext entity object
   */
  public static function CGReferenceExist(VIN: String, lossDate: Date, CGReferenceNumber: String, fileName: String): CarglassDataImportStaging_Ext {
    var query = gw.api.database.Query.make(CarglassDataImportStaging_Ext)

    try {
      query.and(\andCondition -> {
        andCondition.compare(CarglassDataImportStaging_Ext#VIN, Equals, VIN)
        andCondition.compare(CarglassDataImportStaging_Ext#LossDate, Equals, lossDate)
        andCondition.compare(CarglassDataImportStaging_Ext#CarglassReferenceNumber, Equals, CGReferenceNumber)
        andCondition.compare(CarglassDataImportStaging_Ext#FileName, Equals, fileName)
        andCondition.compare(CarglassDataImportStaging_Ext#ClaimNumber, NotEquals, null)
      })
    } catch (e: Exception) {
      _logger.error("Exception in CGReferenceExist() " + e.StackTraceAsString)
    }
    return query.select().AtMostOneRow
  }

  /**
   * function CGInvoiceExist()- Check whether invoice for the carglass reference is already processed or not. If already processed then do insert the invoice data into CarglassInvoiceStaging .
   * @Param correlationID  Vehicle Identification Number
   * @Param fileName File name
   * @Return CarglassInvoiceStaging entity object
   */
  public static function CGInvoiceExist(correlationID: String, fileName: String): CarglassInvoiceStaging_Ext {
    var query = gw.api.database.Query.make(CarglassInvoiceStaging_Ext)

    try {
      query.and(\andCondition -> {
        andCondition.compare(CarglassDataImportStaging_Ext#CorrelationID, Equals, correlationID)
        andCondition.compare(CarglassDataImportStaging_Ext#FileName, Equals, fileName)
      })
    } catch (e: Exception) {
      _logger.error("Exception in CGInvoiceExist() " + e.StackTraceAsString)
    }
    return query.select().AtMostOneRow
  }

  /**
   * function verifyVehicleVIN()- Check in PolicySystem whether Vehicle VIN number is valid or not. If invalid then do not create Claim.
   * @Param policyNumber  Policy Number
   * @Param policyType Policy Type
   * @Param VIN Vehicle Identification Number
   * @Return String Containing VIN validation message
   */
  public static function verifyVehicleVIN(policyNumber: String, policyType: PolicyType, VIN: String): String {
    var isValid = CarglassConstants_Ext.INVALID_VIN
    try {
      gw.transaction.Transaction.runWithNewBundle(\newBundle -> {
        var searchCriteria = new PolicySearchCriteria()
        searchCriteria.PolicyNumber = policyNumber
        searchCriteria.PolicyType = policyType
        var retrievedPolicy = new gw.plugin.pcintegration.pc800.PolicySearchPCPlugin().searchPolicies(searchCriteria)
        if (retrievedPolicy.Summaries.Count != CarglassConstants_Ext.INT_ZERO) {
          retrievedPolicy.Summaries*.Vehicles?.each(\insuredVehicle -> {
            if (insuredVehicle.Vin == VIN){
              isValid = CarglassConstants_Ext.VALID_VIN
            }
          })
        } else {
          isValid = CarglassConstants_Ext.POLICY_NOT_FOUND
        }
      }, Carglass.carglass.integration.user)
    } catch (e: Exception) {
    }
    return isValid
  }

  /**
   * function isDataValid()- Check whether the data is valid or not. If Invalid, then do not create claim.
   * @Param record source file data object
   * @Return HashMap Containing Error messages
   */
  public static function isDataValid(record: Object): HashMap<String, Object> {
    var claimBean = record as CarglassDataBean_Ext
    var errorMessage = new ArrayList()
    var validationMap = new HashMap<String, Object>()
    validationMap.put(CarglassConstants_Ext.IS_PROCESS, true)

    if (claimBean.H3.CarglassReference.Empty){
      _logger.error("${displaykey.Carglass.Label.CarglassReferenceNumber} ${displaykey.Carglass.Error.IsMissing}")
      errorMessage.add("${displaykey.Carglass.Label.CarglassReferenceNumber} ${displaykey.Carglass.Error.IsMissing}")
      validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H2.InsuredName.Empty){
      _logger.error("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA} ${displaykey.Carglass.Label.InsuredName} ${displaykey.Carglass.Error.IsMissing}")
      errorMessage.add("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA} ${displaykey.Carglass.Label.InsuredName} ${displaykey.Carglass.Error.IsMissing}")
      validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H2.InsuredAddress.Empty){
      _logger.error("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA}  ${displaykey.Carglass.Label.InsuredAddress} ${displaykey.Carglass.Error.IsMissing}")
      errorMessage.add("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA}  ${displaykey.Carglass.Label.InsuredAddress} ${displaykey.Carglass.Error.IsMissing}")
      validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H2.InsuredAddressCity.Empty){
      _logger.error("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA} ${displaykey.Carglass.Label.InsuredAddressCity} ${displaykey.Carglass.Error.IsMissing}")
      errorMessage.add("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA}  ${displaykey.Carglass.Label.InsuredAddressCity} ${displaykey.Carglass.Error.IsMissing}")
      validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H3.PolicyNumber.Empty){
      _logger.error("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA}  ${displaykey.Carglass.Label.PolicyNumber} ${displaykey.Carglass.Error.IsMissing}")
      errorMessage.add("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA} ${displaykey.Carglass.Label.PolicyNumber} ${displaykey.Carglass.Error.IsMissing}")
      validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H3.Vin.Empty){
      _logger.error("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA}  ${displaykey.Carglass.Label.VehicleChassisNumber} ${displaykey.Carglass.Error.IsMissing}")
      errorMessage.add("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA}  ${displaykey.Carglass.Label.VehicleChassisNumber} ${displaykey.Carglass.Error.IsMissing}")
      validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
    }

    //retrieve policy details from policy center, if VIN matches process the data else stop the process
    if (!claimBean.H3.PolicyNumber.Empty){
      var policyType: PolicyType
      if (CarglassConstants_Ext.NO_N.equalsIgnoreCase(claimBean.H2.VatLiable)) policyType = typekey.PolicyType.TC_PERSONALAUTO
      else policyType = typekey.PolicyType.TC_BUSINESSAUTO

      var isVINVerified = verifyVehicleVIN(claimBean.H3.PolicyNumber, policyType, claimBean.H3.Vin)
      if (isVINVerified.equalsIgnoreCase(CarglassConstants_Ext.INVALID_VIN)){
        _logger.error("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA}  ${displaykey.Carglass.Label.VehicleChassisNumber} : ${claimBean.H3.Vin} ${displaykey.Carglass.Error.IsInvalid}")
        errorMessage.add("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA}  ${displaykey.Carglass.Label.VehicleChassisNumber} ${claimBean.H3.Vin} ${displaykey.Carglass.Error.IsInvalid}")
        validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
        validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
      }
    }
    if (claimBean.H3.VehicleLicensePlate.Empty){
      _logger.error("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA}  ${displaykey.Carglass.Label.VehicleLicensePlate} ${displaykey.Carglass.Error.IsMissing}")
      errorMessage.add("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA}  ${displaykey.Carglass.Label.VehicleLicensePlate} ${displaykey.Carglass.Error.IsMissing}")
      validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H3.LossDate == null){
      _logger.error("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA}  ${displaykey.Carglass.Label.DateOfLoss} ${displaykey.Carglass.Error.IsMissing}")
      errorMessage.add("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA}  ${displaykey.Carglass.Label.DateOfLoss} ${displaykey.Carglass.Error.IsMissing}")
      validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H3.VehicleMake.Empty){
      _logger.error("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA}  ${displaykey.Carglass.Label.VehicleMake} ${displaykey.Carglass.Error.IsMissing}")
      errorMessage.add("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA}  ${displaykey.Carglass.Label.VehicleMake} ${displaykey.Carglass.Error.IsMissing}")
      validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H3.VehicleModel.Empty){
      _logger.error("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA}  ${displaykey.Carglass.Label.VehicleModel} ${displaykey.Carglass.Error.IsMissing}")
      errorMessage.add("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA}  ${displaykey.Carglass.Label.VehicleModel} ${displaykey.Carglass.Error.IsMissing}")
      validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H3.VehicleYear.Empty){
      _logger.error("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA}  ${displaykey.Carglass.Label.VehicleYearBuilt} ${displaykey.Carglass.Error.IsMissing}")
      errorMessage.add("${displaykey.Carglass.Error.ClaimNotCreated} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference} ${CarglassConstants_Ext.COMMA}  ${displaykey.Carglass.Label.VehicleYearBuilt} ${displaykey.Carglass.Error.IsMissing}")
      validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H0.RepairShopNumber.Empty){
      _logger.warn("${displaykey.Carglass.Warning.warning} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference}  ${CarglassConstants_Ext.COMMA} ${displaykey.Carglass.Label.CarglassRepairShopNumber} ${displaykey.Carglass.Error.IsMissing}")
      errorMessage.add("${displaykey.Carglass.Warning.warning} ${displaykey.Carglass.Label.CarglassReferenceNumber} ${claimBean.H3.CarglassReference}  ${CarglassConstants_Ext.COMMA} ${displaykey.Carglass.Label.CarglassRepairShopNumber} ${displaykey.Carglass.Error.IsMissing}")
      validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
    }
    return validationMap
  }
}