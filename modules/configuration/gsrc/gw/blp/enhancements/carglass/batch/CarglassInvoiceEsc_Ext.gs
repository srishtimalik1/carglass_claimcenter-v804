package gw.blp.enhancements.carglass.batch

uses gw.blp.enhancements.carglass.util.Carglass
uses gw.processes.BatchProcessBase
uses gw.webservice.cc.MaintenanceToolsAPI
uses org.slf4j.LoggerFactory

uses java.lang.Exception

/**
 * Batch updates the Bulk Invoice status to Requesting and updates the Claim's Activities, Exposure and Claim status to Closed
 * Author: Srishti.Malik1
 */
class CarglassInvoiceEsc_Ext extends BatchProcessBase {
  private static var _logger = LoggerFactory.getLogger(CarglassInvoiceEsc_Ext)
  construct() {
    super(BatchProcessType.TC_CARGLASSINVOICEESC_EXT)
  }

  /**
   * function doWork()- executes Bulk Invoice Escalation batch to update bulk invoice status to Requesting state.
   * Update Claim, Exposures and Activities state to Closed state
   */
  override function doWork() {
    try {
      if (TerminateRequested){
        return
      }
      gw.transaction.Transaction.runWithNewBundle(\updateBundle -> {
        new MaintenanceToolsAPI().startBatchProcess(typekey.BatchProcessType.TC_BULKINVOICEESC.Code)
      }, Carglass.carglass.integration.user)

      var claims = gw.api.database.Query.make(CarglassDataImportStaging_Ext).compare(CarglassDataImportStaging_Ext#Status, Equals, typekey.ClaimState.TC_OPEN.Code).select()
      gw.transaction.Transaction.runWithNewBundle(\updateBundle -> {
        claims.each(\claim -> {
          updateBundle.add(claim)
          updateBundle.add(claim.ClaimNumber)
          claim.Status = typekey.ClaimState.TC_CLOSED.Code
          claim.ClaimNumber.Activities.each(\activity -> {
            activity.Status = typekey.ActivityStatus.TC_COMPLETE
          })
          claim.ClaimNumber.Exposures.first().close(typekey.ExposureClosedOutcomeType.TC_COMPLETED, displaykey.Carglass.Claim.CarglassExposureClosed)
          claim.ClaimNumber.close(typekey.ClaimClosedOutcomeType.TC_COMPLETED, displaykey.Carglass.Claim.CarglassClaimClosed)
        })
      }, Carglass.carglass.integration.user)
    } catch (e: Exception) {
      _logger.error("Exception in Carglass Invoice Esc Batch; Exception is ${e.StackTraceAsString}")
    }
  }

  /**
   * Overriden function requestTermination() to terminate the batch execution
   */
  override  function requestTermination(): boolean {
    super.requestTermination()
    return true
  }
}