package gw.blp.enhancements.carglass.batch

uses gw.blp.enhancements.carglass.constants.CarglassConstants_Ext
uses gw.blp.enhancements.carglass.util.Carglass
uses gw.blp.enhancements.carglass.util.CarglassUtil_Ext
uses gw.processes.BatchProcessBase
uses org.slf4j.LoggerFactory

uses java.io.File
uses java.lang.Exception

/**
 * CarglassImportBatch_Ext Batch imports Claim data into ClaimCenter from a fixed length flat file received from Carglass.
 * Author: Srishti.Malik1
 */
class CarglassImportBatch_Ext extends BatchProcessBase {
  private static var LOGGER = LoggerFactory.getLogger("carglassDailyFileLog")
  construct() {
    super(BatchProcessType.TC_CARGLASSIMPORT_EXT)
  }

  /**
   * function doWork()- monitors the input folder and picks all the files. For each picked file, importfile() is processed.
   */
  override function doWork() {
    try {
      var inputFilePath = Carglass.carglass.file.path.inputFolder
      var inputFolder = new File(inputFilePath)
      LOGGER.info("CarglassImportBatch_Ext : Picking files from ${inputFilePath}")
      if (TerminateRequested){
        return
      }
      if (inputFolder.exists()) {
        var fileList = inputFolder.listFiles()
        var fileExists: CarglassDataImportStaging_Ext
        for (var eachFile in fileList) {
          if (TerminateRequested){
            return
          }
          try {
            fileExists = checkInitialCondition(eachFile.Name)
            if (null != fileExists) {
              if (fileExists.Status.equalsIgnoreCase(CarglassConstants_Ext.FILE_COMPLETED)) {


                LOGGER.error("Carglass Import Batch : ${eachFile.Name} ${displaykey.Carglass.Error.FileProcessed} ${fileExists.CreateTime} (yyyy/MM/dd)")
              } else {
                LOGGER.info("${displaykey.Carglass.Info.FileProcessStarts} ${eachFile.Name}")

                if (fileExists.Status.equalsIgnoreCase(CarglassConstants_Ext.PROCESSING_IN_PROGRESS)){
                  new CarglassUtil_Ext().importFile(eachFile)
                  updateFileCompletionStatus(eachFile.Name)
                  LOGGER.info("${displaykey.Carglass.Info.FileProcessStarts} ${eachFile.Name}")
                }
                if (fileExists.Status.equalsIgnoreCase(CarglassConstants_Ext.PROCESSING_ERROR)){
                  updateFileProgressStatus(eachFile.Name, CarglassConstants_Ext.RE_PROCESSING_ERROR)
                  new CarglassUtil_Ext().importFile(eachFile)
                  updateFileCompletionStatus(eachFile.Name)
                  LOGGER.info("${displaykey.Carglass.Info.FileProcessEnds} ${eachFile.Name}")
                }
              }
            } else {
              LOGGER.info("${displaykey.Carglass.Info.FileProcessStarts} ${eachFile.Name}")
              updateFileProgressStatus(eachFile.Name, CarglassConstants_Ext.PROCESSING_IN_PROGRESS)
              new CarglassUtil_Ext().importFile(eachFile)
              updateFileCompletionStatus(eachFile.Name)
              LOGGER.info("${displaykey.Carglass.Info.FileProcessEnds} ${eachFile.Name}")
            }
          } catch (e: Exception) {
            LOGGER.error("Carglass Import Batch : Exception occurred while processing the file: ${eachFile}. Exception is- ${e.StackTraceAsString}")
            updateFileProgressStatus(eachFile.Name, CarglassConstants_Ext.PROCESSING_ERROR)
            continue
          }
        }
      } else {
        LOGGER.error("Carglass Import Batch : ${inputFilePath} is not accessible")
      }
    } catch (e: Exception) {
      LOGGER.error("CarglassImportBatch_Ext : ${e.StackTraceAsString}")
    }
  }

  /**
   * function checkInitialCondition- Check whether the file is already processed or not. If already processed, then do not process the source file again
   * @Param fileName Name of the source file.
   * @Return CarglassDataImportStaging_Ext Entity object
   */
  private function checkInitialCondition(eachFile: String): CarglassDataImportStaging_Ext {
    var query = gw.api.database.Query.make(CarglassDataImportStaging_Ext)
    try {
      query.and(\andCondition -> {
        andCondition.compare(CarglassDataImportStaging_Ext#FileName, Equals, eachFile)
        andCondition.compare(CarglassDataImportStaging_Ext#CarglassReferenceNumber, Equals, null)
      }).select()
    } catch (e: Exception) {
      LOGGER.error("Exception in checkInitialCondition: ${e.StackTraceAsString}")
    }
    return query.select().FirstResult
  }

  /**
   * function updateFileCompletionStatus- Updates the file processing status to Completed.
   * @Param fileName Name of the source file.
   */
  private function updateFileCompletionStatus(eachFile: String) {
    try {

      gw.transaction.Transaction.runWithNewBundle(\transactionBundle -> {
        var file = gw.api.database.Query.make(CarglassDataImportStaging_Ext).and(\andCondition -> {
          andCondition.compare(CarglassDataImportStaging_Ext#FileName, Equals, eachFile)
          andCondition.compare(CarglassDataImportStaging_Ext#Status, Equals, CarglassConstants_Ext.PROCESSING_IN_PROGRESS)
        }).select().AtMostOneRow
        var updateFile = transactionBundle.add(file)
        file.Status = CarglassConstants_Ext.FILE_COMPLETED
      }, Carglass.carglass.integration.user)
    } catch (e: Exception) {
      LOGGER.error("Exception in updateFileCompletionStatus: ${e.StackTraceAsString}")
    }
  }

  /**
   * function updateFileProgressStatus- Updates the file processing status to
   * PROCESSING_IN_PROGRESS if file processing initiated or
   * PROCESSING_ERROR if an error occurred durinf file processing
   * @Param fileName Name of the source file.
   * @Param status File progress Status
   */
  private function updateFileProgressStatus(eachFile: String, status: String) {
    try {
      gw.transaction.Transaction.runWithNewBundle(\transactionBundle -> {
        var stgData: CarglassDataImportStaging_Ext
        if (status.equalsIgnoreCase(CarglassConstants_Ext.PROCESSING_IN_PROGRESS)){
          stgData = transactionBundle.add(new CarglassDataImportStaging_Ext())
        }
        if (status.equalsIgnoreCase(CarglassConstants_Ext.PROCESSING_ERROR)){

          stgData = gw.api.database.Query.make(CarglassDataImportStaging_Ext).and(\andCondition -> {
            andCondition.compare(CarglassDataImportStaging_Ext#FileName, Equals, eachFile)
            andCondition.compare(CarglassDataImportStaging_Ext#Status, Equals, CarglassConstants_Ext.PROCESSING_IN_PROGRESS)
          }).select().AtMostOneRow
          transactionBundle.add(stgData)
        }
        if (status.equalsIgnoreCase(CarglassConstants_Ext.RE_PROCESSING_ERROR)){

          stgData = gw.api.database.Query.make(CarglassDataImportStaging_Ext).and(\andCondition -> {
            andCondition.compare(CarglassDataImportStaging_Ext#FileName, Equals, eachFile)
            andCondition.compare(CarglassDataImportStaging_Ext#Status, Equals, CarglassConstants_Ext.PROCESSING_ERROR)
          }).select().AtMostOneRow
          transactionBundle.add(stgData)
          status = CarglassConstants_Ext.PROCESSING_IN_PROGRESS
        }

        stgData.FileName = eachFile
        stgData.Status = status
      }, Carglass.carglass.integration.user)
    } catch (e: Exception) {
      LOGGER.error("Exception in updateFileProgressStatus: ${e.StackTraceAsString}")
    }
  }

  /**
   * Overriden function requestTermination() to terminate the batch execution
   */
  override  function requestTermination(): boolean {
    super.requestTermination()
    return true
  }
}