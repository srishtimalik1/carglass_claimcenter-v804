package gw.blp.enhancements.carglass.acordmapper

uses gw.blp.enhancements.carglass.util.Carglass
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory

uses java.lang.Exception
uses java.lang.Integer
uses java.math.BigDecimal
uses java.util.ArrayList

/**
 * CarglassBulkInvoiceMapper_Ext prepares the data to create Bulk Recovery for all the claims in each file sent by the Carglass
 * Author: Srishti.Malik1
 *
 */
public class CarglassBulkRecoveryMapper_Ext {
  public static var _bulkRecoveryClaimList: ArrayList<CarglassDataImportStaging_Ext>
  private static var _payer: Contact
  private static var LOGGER: Logger = LoggerFactory.getLogger(CarglassClaimMapper_Ext)
  /**
   * function createBulkRecovery()- create and populate the BulkRecoveryItem_Ext entity object..
   * @Param CheckNumber  Check Number sent in the Flat file.
   */
  public function createBulkRecovery(CheckNumber: String) {
    try {
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        var bulkRecoveryExtEnh = gw.acc.bulkrecovery.BulkRecoveriesHelper_Ext.newBulkRecovery()
        _bulkRecoveryClaimList.each(\claim -> {
          var recoveryItem = new BulkRecoveryItem_Ext()
          var amt = claim.CarglassInvoices.where(\elt -> (elt.Amount as Integer) < 0).sum(\elt -> elt.Amount as BigDecimal)
          recoveryItem.Amount = new gw.api.financials.CurrencyAmount ((amt * - 1), typekey.Currency.TC_EUR)
          recoveryItem.Claim = claim.ClaimNumber
          recoveryItem.ClaimNumber = claim.ClaimNumber.ClaimNumber
          recoveryItem.Exposure = claim.ClaimNumber.Exposures.first()
          recoveryItem.ReserveLine = claim.ClaimNumber.ReserveLines.first()
          recoveryItem.Currency = typekey.Currency.TC_EUR
          recoveryItem.CostType = typekey.CostType.TC_AOEXPENSE
          recoveryItem.CostCategory = typekey.CostCategory.TC_AUTOGLASS
          recoveryItem.RecoveryCategory = typekey.RecoveryCategory.TC_SALVAGE
          recoveryItem.LineCategory = typekey.LineCategory.TC_OTHER
          bulkRecoveryExtEnh.addToRecoveryItems(recoveryItem)
        })
        bulkRecoveryExtEnh.Payer = _payer
        bulkRecoveryExtEnh.CheckNumber = CheckNumber
        bulkRecoveryExtEnh.DefaultRecoveryCategory = typekey.RecoveryCategory.TC_SALVAGE
        bulkRecoveryExtEnh.validateAndSubmit()
        bulkRecoveryExtEnh.recalculateTotalAmount()
      }, Carglass.carglass.integration.user)
    } catch (e: Exception) {
      LOGGER.error("Exception in createBulkRecovery: ${e.StackTraceAsString}")
    }
  }

  /**
   * function prepareBulkRecoveryItem()- create and populate the BulkRecoveryItem_Ext entity object.
   * @Param claim  Claim whose recovery is to be created.
   * @Param abContact Contact fetched from Contact Manager
   */
  public static function prepareBulkRecoveryItem(claim: CarglassDataImportStaging_Ext, abContact: Contact) {
    try {
      if (_bulkRecoveryClaimList.Count == 0){
        _bulkRecoveryClaimList = new ArrayList<CarglassDataImportStaging_Ext>()
      }
      _payer = abContact
      _bulkRecoveryClaimList.add(claim)
    } catch (e: Exception) {
      LOGGER.error("Exception in prepareBulkRecoveryItem ${e.StackTraceAsString}")
    }
  }
}