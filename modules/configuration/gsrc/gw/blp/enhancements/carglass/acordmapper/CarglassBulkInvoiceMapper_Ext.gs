package gw.blp.enhancements.carglass.acordmapper

uses gw.blp.enhancements.carglass.util.Carglass
uses gw.plugin.contact.ab800.ABContactSystemPlugin
uses gw.plugin.contact.search.ContactSearchFilter
uses gw.webservice.cc.cc801.dto.BulkInvoiceDTO
uses gw.webservice.cc.cc801.dto.BulkInvoiceItemDTO
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory

uses java.lang.Exception
uses java.lang.Integer
uses java.math.BigDecimal
uses java.util.ArrayList
uses org.apache.commons.lang3.StringUtils

/**
 * CarglassBulkInvoiceMapper_Ext prepares the Bulk Invoice DTO which is used to create the bulk invoice for all the claims in each file sent by the Carglass
 * Author: Srishti.Malik1
 *
 */
public class CarglassBulkInvoiceMapper_Ext {
  private static var LOGGER: Logger = LoggerFactory.getLogger(CarglassBulkInvoiceMapper_Ext)
  public static var _claimStagingList: List<CarglassDataImportStaging_Ext>
  /**
   * function prepareBulkInvoiceDTO()- Prepared BulkInvoiceDTO for all the claims sent in flat file having status as Draft in carglass staging table.
   * Fetches the Check Payee Details  from Contact Manager, with search criteria as Contact's Tax ID
   * @Param taxID Contact's Tax ID to be used to serach contact from contact manager
   * @Param fileName Source file name which is in process
   * @return   BulkInvoiceDTO having details of all the claims which are in draft status
   */
  public function prepareBulkInvoiceDTO(taxID: String, fileName: String, bankAccountNumber :String): BulkInvoiceDTO {
    var invoiceDTO: BulkInvoiceDTO
    try {
      var abContact: Contact
      var invoiceItemDTO = new BulkInvoiceItemDTO()
      var newItemsArray = new ArrayList<BulkInvoiceItemDTO>()
      _claimStagingList = new ArrayList<CarglassDataImportStaging_Ext>()
      var payeeContact  : Contact
      var claimStagingData = gw.api.database.Query.make(CarglassDataImportStaging_Ext)
          .and(\andCondition -> {
            andCondition.compare(CarglassDataImportStaging_Ext#FileName, Equals, fileName)
            andCondition.compare(CarglassDataImportStaging_Ext#Status, Equals, typekey.ClaimState.TC_DRAFT.Code)
            andCondition.compare(CarglassDataImportStaging_Ext#ClaimNumber, NotEquals, null)
          }).select()
      if (claimStagingData.HasElements){
        _claimStagingList = new ArrayList<CarglassDataImportStaging_Ext>()
        _claimStagingList = claimStagingData.toList()
        invoiceDTO = new BulkInvoiceDTO()
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          var searchCriteria = new ContactSearchCriteria()
          searchCriteria.TaxID = taxID
          searchCriteria.ContactSubtype = typekey.Contact.TC_COMPANYVENDOR
          var contactSearchResult = new ABContactSystemPlugin().searchContacts(searchCriteria, new ContactSearchFilter())
          if (null != contactSearchResult && contactSearchResult.NumberOfResults > 0){
            abContact = contactSearchResult.getContacts().first()
          }
          payeeContact  = abContact
        }, Carglass.carglass.integration.user)
        invoiceDTO.PaymentMethod = typekey.PaymentMethod.TC_EFT
        invoiceDTO.BankAccountType = typekey.BankAccountType.TC_CHECKING
        if (null != abContact.PublicID) {
          invoiceDTO.AccountName = abContact.EFTRecords.first().AccountName != null ? abContact.EFTRecords.first().AccountName : "Carglass"
          invoiceDTO.BankAccountNumber = abContact.EFTRecords.first().BankAccountNumber  != null ?  abContact.EFTRecords.first().BankAccountNumber : bankAccountNumber
          invoiceDTO.BankRoutingNumber = abContact.EFTRecords.first().BankRoutingNumber  != null ?   abContact.EFTRecords.first().BankRoutingNumber : "BBRUBEBB"
          invoiceDTO.PayeeID = abContact.PublicID
        } else {
          invoiceDTO.PayeeID = payeeContact.PublicID
          invoiceDTO.AccountName = payeeContact.EFTRecords.first().AccountName != null ? payeeContact.EFTRecords.first().AccountName : "Carglass"
          invoiceDTO.BankAccountNumber = payeeContact.EFTRecords.first().BankAccountNumber  != null ?  payeeContact.EFTRecords.first().BankAccountNumber : bankAccountNumber
          invoiceDTO.BankRoutingNumber = payeeContact.EFTRecords.first().BankRoutingNumber  != null ?   payeeContact.EFTRecords.first().BankRoutingNumber : "BBRUBEBB"
        }

        CarglassBulkRecoveryMapper_Ext._bulkRecoveryClaimList = new ArrayList<CarglassDataImportStaging_Ext>()
        claimStagingData.each(\eachClaim -> {
          var invoiceAmount = eachClaim.CarglassInvoices?.where(\elt -> elt.Amount as Integer > 0)?.sum(\elt -> elt.Amount as BigDecimal)
          if (invoiceAmount != null && invoiceAmount > 0){
            validateReserveAmount(eachClaim)
            invoiceItemDTO = prepareBulkInvoiceItemDTO(eachClaim)
            newItemsArray.add(invoiceItemDTO)
          }
          var recoveryAmount = eachClaim.CarglassInvoices?.where(\elt -> (elt.Amount as Integer) < 0)?.sum(\elt -> elt.Amount as BigDecimal)
          if (recoveryAmount != null && recoveryAmount < 0){
            CarglassBulkRecoveryMapper_Ext.prepareBulkRecoveryItem(eachClaim, abContact)
          }
        })
        invoiceDTO.NewInvoiceItems = newItemsArray as BulkInvoiceItemDTO[]
      }
    } catch (e: Exception) {
      LOGGER.error("Exception in prepareBulkInvoiceDTO: ${e.StackTraceAsString}")
    }
    return invoiceDTO
  }

  /**
   * function prepareBulkInvoiceItemDTO()- Prepared BulkInvoiceItemDTO for each claim
   * @Param CarglassDataImportStaging_Ext each claim whopse invoice is to be created
   * @return   BulkInvoiceItemDTO having details of each claim whose invoice is to be created
   */
  private function prepareBulkInvoiceItemDTO(eachData: CarglassDataImportStaging_Ext): BulkInvoiceItemDTO {
    var invoiceItemDTO = new BulkInvoiceItemDTO()
    try {
      invoiceItemDTO.ClaimID = eachData.ClaimNumber.PublicID
      var amount = eachData.CarglassInvoices.where(\elt -> elt.Amount as Integer > 0).sum(\elt -> elt.Amount as BigDecimal)
      var currAmount = gw.api.financials.CurrencyAmount.getStrict(amount, Currency.TC_EUR)
      invoiceItemDTO.Amount = currAmount
      invoiceItemDTO.CostCategory = CostCategory.TC_AUTOGLASS
      invoiceItemDTO.Status = BulkInvoiceItemStatus.TC_DRAFT
      invoiceItemDTO.ReservingCurrency = typekey.Currency.TC_EUR
      invoiceItemDTO.CostType = CostType.TC_AOEXPENSE
      invoiceItemDTO.PaymentType = PaymentType.TC_FINAL
      if (eachData.ClaimNumber.Exposures.Count > 0){
        invoiceItemDTO.ExposureID = eachData.ClaimNumber.Exposures.first().PublicID
      }
      invoiceItemDTO.DateOfService = eachData.ReportedDate != null ? eachData.ReportedDate : eachData.LossDate
      invoiceItemDTO.Description =StringUtils.join(eachData.CarglassInvoices.where(\elt -> elt.Amount as Integer > 0)*.CorrelationID, ",")
      invoiceItemDTO.NonEroding = false
    } catch (e: Exception) {
      LOGGER.error("Exception in prepareBulkInvoiceItemDTO: ${e.StackTraceAsString}")
    }
    return invoiceItemDTO
  }

  /**
   * function validateReserveAmount()- validate the reserve amount of a claim. If the current amount is not equivalent to the total amount then a new Reserve will be created of total amount.
   * @Param CarglassDataImportStaging_Ext each claim whopse invoice is to be created
   */
  private function validateReserveAmount(eachData: CarglassDataImportStaging_Ext) {
    try {

      var totalAmount = eachData.CarglassInvoices.where(\elt -> elt.Amount as Integer > 0).sum(\elt -> elt.Amount as BigDecimal)
      var totalCurrAmount = gw.api.financials.CurrencyAmount.getStrict(totalAmount, Currency.TC_EUR)
      var amount: BigDecimal
      if (eachData.ClaimNumber.Exposures.Count > 0){
        amount = eachData.ClaimNumber.Exposures.first().OpenReserves.Amount
        if (totalCurrAmount.Amount != amount){
          gw.transaction.Transaction.runWithNewBundle(\bundle -> {
            var su = User.finder.findUserByUserName(Carglass.carglass.integration.user)
            var exp = eachData.ClaimNumber.Exposures.first()
            var initialReserve = exp.ReserveLines.first()
            bundle.add(initialReserve)
            initialReserve.setAvailableReserves(totalCurrAmount.Amount, su)
          }, Carglass.carglass.integration.user)
        }
      }
    } catch (e: Exception) {
      LOGGER.error("Exception in validateReserveAmount: ${e.StackTraceAsString}")
    }
  }
}